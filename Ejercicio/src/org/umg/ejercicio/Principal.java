package org.umg.ejercicio;

import java.util.Scanner;

/**
 * Created by alumno on 5/07/2017.
 */
public class Principal {

    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        int canal;
        boolean salir = false;
        int opcion;

        Televisor t = new Televisor();
        t.setMarca("Panasonic");
        t.setPulgadas((float) 18.5);
        t.setPlano("Yes");

        Canal c1 = new Canal();
        c1.setNumero(10);
        c1.setTematica("Series");
        c1.setPermitido("Yes");
        t.addCanal(c1);

        Canal c2 = new Canal();
        c2.setNumero(15);
        c2.setTematica("Deportes");
        c2.setPermitido("Yes");
        t.addCanal(c2);

        Canal c3 = new Canal();
        c3.setNumero(20);
        c3.setTematica("Terror");
        c3.setPermitido("No");
        t.addCanal(c3);

        Canal c4 = new Canal();
        c4.setNumero(25);
        c4.setTematica("Noticias");
        c4.setPermitido("No");
        t.addCanal(c4);

        Canal c5 = new Canal();
        c5.setNumero(30);
        c5.setTematica("Dibujos Animados");
        c5.setPermitido("Yes");
        t.addCanal(c5);

        while (!salir) {
            System.out.println("Seleccione una de las siguientes opciones: ");
            System.out.println("1. Listar datos televisor");
            System.out.println("2. Listar los datos de los canales");
            System.out.println("3. Analizar si un canal es permitido para los niños");
            System.out.println("4. Salir");
            opcion = sn.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("*****************************");
                    System.out.println("Marca: " + t.getMarca());
                    System.out.println("Pulgadas: " + t.getPulgadas());
                    System.out.println("Es plano?: " + t.getPlano());
                    System.out.println("*****************************");
                    break;
                case 2:
                    System.out.println("*****************************");
                    t.listarCanales();
                    System.out.println("*****************************");
                    break;
                case 3:
                    System.out.println("*****************************");
                    t.verificarInfantil(c1);
                    System.out.println("*****************************");
                    break;
                case 4:
                    System.out.println("*****************************");
                    System.out.println("Adiós");
                    System.out.println("*****************************");
                    salir = true;
                    break;
            }
        }
    }
    }

