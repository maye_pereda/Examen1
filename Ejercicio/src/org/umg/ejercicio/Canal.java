package org.umg.ejercicio;

/**
 * Created by alumno on 5/07/2017.
 */
public class Canal {
    private int numero;
    private String tematica;
    private String permitido;

    public Canal() {
    }

    public Canal(int numero, String tematica, String permitido) {
        this.numero = numero;
        this.tematica = tematica;
        this.permitido = permitido;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    public String getPermitido() {
        return permitido;
    }

    public void setPermitido(String permitido) {
        this.permitido = permitido;
    }
}
