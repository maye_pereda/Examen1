package org.umg.ejercicio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alumno on 5/07/2017.
 */
public class Televisor {
    private String marca;
    private float pulgadas;
    private String plano;
    List<Canal> canal = new ArrayList<>();

    public Televisor() {
    }

    public Televisor(String marca, float pulgadas, String plano, List<Canal> canal) {
        this.marca = marca;
        this.pulgadas = pulgadas;
        this.plano = plano;
        this.canal = canal;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPulgadas() {
        return pulgadas;
    }

    public void setPulgadas(float pulgadas) {
        this.pulgadas = pulgadas;
    }

    public String getPlano() {
        return plano;
    }

    public void setPlano(String plano) {
        this.plano = plano;
    }

    public List<Canal> getCanal() {
        return canal;
    }

    public void setCanal(List<Canal> canal) {
        this.canal = canal;
    }

    public void addCanal(Canal c){
        canal.add(c);
    }

    public int tamanioLista(){
        return canal.size();
    }

    public void verificarInfantil(Canal canal1){
        for (Canal c:canal
             ) {
            if (c.getNumero() == canal1.getNumero()) {
                if (canal1.getPermitido().equals("Yes") && (canal1.getTematica().equals("Dibujos Animados"))) {
                    System.out.println("Si está permitido para menores y su temática es: " + canal1.getTematica());
                } else {
                    System.out.println("Este canal no es permitido para menores porque su temática es: " + canal1.getTematica());
                }
            }
        }
        }

    public void listarCanales(){
        for (Canal c:canal
             ) {
            System.out.println("Número: "+c.getNumero()+"-Temática: "+c.getTematica()+"-Permitido para niños?: "+c.getPermitido());
        }
    }
}
